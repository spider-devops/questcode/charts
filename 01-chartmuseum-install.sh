helm install museum stable/chartmuseum --set env.open.DISABLE_API=false --set service.type=ClusterIP -n devsecops
helm plugin install https://github.com/chartmuseum/helm-push

helm repo add actarlab http://chart-museum.actar.lab

helm lint frontend
helm push frontend/ questcode
helm lint backend-user
helm push backend-user/ questcode
helm lint backend-scm
helm push backend-scm/ questcode
